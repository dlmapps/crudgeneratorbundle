# CrudGeneratorBundle
Symfony3 CRUD generator bundle.

Designed to change the default behavior , but extending from [SensioGeneratorBundle](https://github.com/sensio/SensioGeneratorBundle) .


## Features
* Views repository is in the bundle, and not in app
* Bootstrap use

## Installation
This bundle is tested with Symfony 3.3.4.

### Using composer

#### Symfony >= 2.8 

    composer require DLMApps/Tools/crud-generator-bundle

Register the CRUD and filter bundles in your `AppKernel.php`:

        new DLMApps\Tools\CrudGeneratorBundle\DLMAppsToolsCrudGeneratorBundle(),

Install the assets.
```sh
php bin/console assets:install --symlink
```

For the bootstrap theme of the forms, add this to your `app/config/config.yml`
```yaml
twig:
    form_themes:
	- 'bootstrap_3_layout.html.twig' 

```


## Dependencies

This bundle extends [SensioGeneratorBundle](https://github.com/sensio/SensioGeneratorBundle). 


## Usage

Use the following command from console:
```sh
php bin/console dlmappstools:doctrine:generate:crud 
```
And follow the wizard steps.

## Author

Delphine Lemire - delphine.lemire at gmail dot com

## License

CrudGeneratorBundle is licensed under the MIT License.
