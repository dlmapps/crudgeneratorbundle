<?php

/*
 * This file is part of the DLMApps/Tools/CrudGeneratorBundle
 *
 * (c) Delphine Lemire <delphine.lemire@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */


namespace DLMApps\Tools\CrudGeneratorBundle\Generator;


use Sensio\Bundle\GeneratorBundle\Generator\DoctrineCrudGenerator;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpKernel\Bundle\BundleInterface;
use Doctrine\ORM\Mapping\ClassMetadataInfo;
use Doctrine\Common\Inflector\Inflector;

/**
 * Generates a CRUD controller.
 *
 * @author Delphine Lemire <delphine.lemire@gmail.com>
 */
class DlmappstoolsDoctrineCrudGenerator extends DoctrineCrudGenerator
{
    /**
     * Generates the _form.html.twig template in the final bundle.
     *
     * @param string $dir The path to the folder that hosts templates in the bundle
     */
    protected function generateFormView($dir)
    {
        $this->renderFile('crud/views/_form.html.twig.twig', $dir.'/_form.html.twig', array(
            'bundle' => $this->bundle->getName(),
            'entity' => $this->entity,
            'entity_singularized' => $this->entitySingularized,
            'route_prefix' => $this->routePrefix,
            'route_name_prefix' => $this->routeNamePrefix,
            'fields' => $this->metadata->fieldMappings,
        ));
    }
  
    /**
     * Generate the CRUD controller.
     *
     * @param BundleInterface   $bundle           A bundle object
     * @param string            $entity           The entity relative class name
     * @param ClassMetadataInfo $metadata         The entity class metadata
     * @param string            $format           The configuration format (xml, yaml, annotation)
     * @param string            $routePrefix      The route name prefix
     * @param bool              $needWriteActions Whether or not to generate write actions
     * @param bool              $forceOverwrite   Whether or not to overwrite the controller
     *
     * @throws \RuntimeException
     */
    public function generate(BundleInterface $bundle, $entity, ClassMetadataInfo $metadata, $format, $routePrefix, $needWriteActions, $forceOverwrite)
    {
        
             
       $path_bundle = $bundle->getPath();
       
       
       $old_dir = sprintf('%s/Resources/views/%s', $this->rootDir, strtolower($entity));
       $new_dir = sprintf('%s/Resources/views/%s', $path_bundle, strtolower($entity));
       
       
       parent::generate($bundle, $entity, $metadata, $format, $routePrefix, $needWriteActions, $forceOverwrite);
       
       $dir = sprintf('%s/Resources/views/%s', $this->rootDir, strtolower($entity));
       
       if (!file_exists($dir)) {
           self::mkdir($dir);
       }
       $this->generateFormView($dir);
       
       if (!$forceOverwrite && file_exists($new_dir)) {
           throw new \RuntimeException('Unable to generate the view as it already exists.');
       }
       
       $this->filesystem->rename($old_dir, $new_dir,$forceOverwrite);
    
       
    }

    
}
