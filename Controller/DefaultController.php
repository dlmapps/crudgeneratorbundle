<?php

namespace DLMApps\Tools\CrudGeneratorBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('DLMAppsToolsCrudGeneratorBundle:Default:index.html.twig');
    }
}
